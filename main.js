//Конструкцію try/catch доцільно використовувати завжди коли ми працюємо з api або отримуємо/обробляємо якійсь дані;
//Як відомо,JS під час помилки перестає працювати. Завдяки цій конструкції ми можем перехопити помилки, вивести їх і таким чином забезпечити подальшу роботу сайту 


const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');

const ul = document.createElement('ul');

root.append(ul);

function filter (arr, list, value){
    arr.forEach(el => {
        try {
            const keys = Object.keys(el);
            if(keys.length >=3){
                const li = document.createElement('li');
                for (const e in el) {
                    li.append(`${e} : ${el[e]}` + ' ');
                    list.append(li)
                }
            }else{
                const errorElement = value.filter(element => !keys.includes(element))[0];
                throw new Error('Not found: ' + `${errorElement}`);
            }
        } catch (error) {
            console.log(error);
        }
    });
}

filter(books, ul, ['author', 'name', 'price']);
